-- 1. Вивести всі товари (таблиця Production.Product) чорного кольору (color), 
-- відсортувати за спаданням ціни (ListPrice)
SELECT * 
FROM Production.Product
WHERE color = 'Black'
ORDER BY ListPrice DESC

-- 2. Вивести всі червоні та сині товари назва яких починається на літеру 'L', 
-- відсортувати по розміру за спаданням та ціною за зростанням
SELECT * 
FROM Production.Product
WHERE Color IN ('Red', 'Blue') 
  AND Name Like 'L%'
ORDER BY Size DESC, ListPrice ASC

-- 3. Вивести регіони доставки (з таблиці Sales.SalesTerritory), 
-- призначені Європі, відсортовані за назвою країни.
SELECT * 
FROM Sales.SalesTerritory
WHERE "group" = 'Europe'
ORDER BY Name

-- 4. Вивести регіони доставки, відсортовані по групі регіонів (group) 
-- за спаданням та назвами країн (name) за зростанням
SELECT * 
FROM Sales.SalesTerritory
ORDER BY "group" DESC, Name ASC

-- 5. Вивести замовлення (з таблиці Sales.SalesOrderHeader), 
-- призначені для областей з TerritoryID: 7,8,9
SELECT * 
FROM Sales.SalesOrderHeader 
WHERE TerritoryID IN (7,8,9)

-- 6. Вивести останні 10 замовлень, які належать до областей з ідентифікаторами: 
-- 7,8,9 із загальною кількістю товарів (SubTotal) менше ніж 100
SELECT * 
FROM Sales.SalesOrderHeader 
WHERE TerritoryID IN (7,8,9) 
  AND SubTotal < 100
ORDER BY OrderDate -- ! TODO

-- 7. Вивести 10 замовлень з найбільшою кількістю товарів (SubTotal), 
-- які належать до області (TerritoryID) = 7
SELECT * 
FROM Sales.SalesOrderHeader 
WHERE TerritoryID = 7
ORDER BY SubTotal DESC -- ! TODO
LIMIT 10

-- 8. Вивести замовлення, призначені для області 7 (TerritoryID), 
-- без присвоєного номера кредитної картки (CreditCardID)
SELECT *
FROM Sales.SalesOrderHeader 
WHERE TerritoryID = 7 
  AND CreditCardID IS NULL
  
-- 9. Вивести замовлення, призначені для області 7 (TerritoryID), 
-- із призначеним номером кредитної картки (CreditCardID)
SELECT *
FROM Sales.SalesOrderHeader 
WHERE TerritoryID = 7 
  AND CreditCardID IS NOT NULL
  
-- 10. Вивести замовлення 2011 року, відсортовані за датою замовлення (OrderDate)
SELECT * 
FROM Sales.SalesOrderHeader 
WHERE OrderDate BETWEEN '20110101' AND '20111231'
ORDER BY OrderDate
